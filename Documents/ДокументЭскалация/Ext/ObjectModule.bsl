﻿
Процедура ОбработкаПроведения(Отказ, Режим)
	//{{__КОНСТРУКТОР_ДВИЖЕНИЙ_РЕГИСТРОВ
	// Данный фрагмент построен конструктором.
	// При повторном использовании конструктора, внесенные вручную изменения будут утеряны!!!

	// регистр РегистрЭскалация Приход
	Движения.РегистрЭскалация.Записывать = Истина;
	Движения.РегистрЭскалация.Очистить();
	Для Каждого ТекСтрокаТабличнаяЧасть1 Из ТабличнаяЧасть1 Цикл
		Движение = Движения.РегистрЭскалация.Добавить();
		Движение.ВидДвижения = ВидДвиженияНакопления.Приход;
		Движение.Период = Дата;
		Движение.Измерение1 = ТекСтрокаТабличнаяЧасть1.Реквизит1;
		Движение.Ресурс1 = 1;
	КонецЦикла;
	
	Движения.Записать();

	//}}__КОНСТРУКТОР_ДВИЖЕНИЙ_РЕГИСТРОВ
КонецПроцедуры
